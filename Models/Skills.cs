﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon_manager.Models
{
    public class Skills
{
    public long Id { get; set; }

    public string Athletics { get; set; }
    public string Acrobatics { get; set; }

    public string Combat { get; set; }

    public string Stealth { get; set; }

    public string Perception { get; set; }

    public string Focus { get; set; }
}
}
