﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon_manager.Models
{
    internal abstract class Capability
    {
        public abstract string Name { get; }
    }

    enum CapabilityNames
    {
        OVERLAND,
        SWIM,
        HIGH_JUMP,
        LONG_JUMP,
        POWER,
        STEALTH,
    }
}
