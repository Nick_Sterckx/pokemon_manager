﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon_manager.Models
{
    public class Ability
{
    public long Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public Frequency Frequency { get; set; }

    public ActionType ActionType { get; set; }
}

    public enum ActionType
    {
        FREE,
        SHIFT,
        STANDARD,
        FULL
    }
}
