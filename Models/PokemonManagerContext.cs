﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon_manager.Models
{
    public class PokemonManagerContext :DbContext
{
        public PokemonManagerContext(DbContextOptions<PokemonManagerContext> options)
           : base(options)
        {
        }

        public DbSet<Pokemon> PokemonItems { get; set; }
        public DbSet<Move> MoveItems { get; set; }
        public DbSet<Ability> AbilityItems { get; set; }
    }
}
