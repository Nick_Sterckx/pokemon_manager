﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;

namespace Pokemon_manager.Models
{
    public class Pokemon
    {
        public long Id { get; set; }
        public int Pokedex_id { get; set; }

        public HashSet<EvolutionInfo> PossibleEvolutions { get; set; }

        public string Name { get; set; }

        public Type Type1 { get; set; }

        public Type Type2 { get; set; }

        public int Hp { get; set; }
        public int Att { get; set; }
        public int Def { get; set; }
        public int Satt { get; set; }
        public int Sdef { get; set; }
        public int Spd { get; set; }

        public HashSet<AbilityLink> Abilities { get; set; }

        public string Height { get; set; }

        public string Weight { get; set; }

        public float GenderRatio { get; set; }

        public HashSet<Move> LevelMoves { get; set; }
        public HashSet<Move> TMMoves { get; set; }
        public HashSet<Move> EggMove { get; set; }
        public HashSet<Move> TutorMove { get; set; }

        public EggGroup EggGroup1 { get; set; }
        public EggGroup? EggGroup2 { get; set; }

        [NotMapped]
        public HashSet<Diet> Diets
        {
            get
            {
                HashSet<Diet> result = new HashSet<Diet>();
                if (_Diets != null)
                {
                    String[] Split = _Diets.Split(";");
                    foreach (var s in Split)
                    {
                        result.Add(Enum.Parse<Diet>(s));
                    }
                }

                return result;
            }
            set
            {
                List<String> result = new List<String>();
                foreach (var diet in value)
                {
                    result.Add(diet.ToString());
                }

                _Diets = String.Join(";", result);
            }
        }

        public String _Diets { get; set; }

        [NotMapped]
        public HashSet<Habitat> Habitats
        {
            get
            {

                HashSet<Habitat> result = new HashSet<Habitat>();
                if (_Habitats != null)
                {
                    String[] Split = _Habitats.Split(";");
                    foreach (var s in Split)
                    {
                        result.Add(Enum.Parse<Habitat>(s));
                    }
                }
                return result;
            }
            set
            {
                List<String> result = new List<String>();
                foreach (var habitat in value)
                {
                    result.Add(habitat.ToString());
                }

                _Habitats = String.Join(";", result);
            }
        }

        private String _Habitats { get; set; }

        public Skills Skills { get; set; }

        // todo: add capabilities
    }

    public class AbilityLink
    {
        public long Id { get; set; }
        public AbilityType Type { get; set; }
        public Ability Ability { get; set; }
    }

    public class EvolutionInfo
    {
        public long Id { get; set; }

        public long Pokedex_id { get; set; }

        public EvolutionType EvolutionType { get; set; }

        public string Info { get; set; }
    }

    public enum EvolutionType
    {
        LEVEL,
        STONE,
        OTHER
    }

    public enum AbilityType
    {
        BASIC,
        ADVANCED,
        HIGH
    }

    public enum EggGroup
    {
        MONSTER,
        WATER_1,
        BUG,
        FLYING,
        FIELD,
        FAIRY,
        GRASS,
        HUMANLIKE,
        WATER_3,
        MINERAL,
        AMORPHOUS,
        WATER_2,
        DITTO,
        DRAGON,
        UNDISCOVERED

    }

    public enum Diet
    {
        CARNIVORE,
        HERBIVORE,
        OMNIVORE,
        ERGOVORE,
        TERRAVORE,
        NULLIVORE
    }

    public enum Habitat
    {
        OCEAN,
        ARCTIC,
        TUNDRA,
        CAVE,
        TAIGA,
        FOREST,
        URBAN,
        GRASSLAND,
        BEACH,
        DESERT,
        MOUNTAIN,
        RAINFOREST
    }
}
