﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon_manager.Models 
{
    public class Move
{
    public long Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public Type Type { get; set; }

    public Frequency Frequency { get; set; }
    public int? FrequencyTimes { get; set; }

    public int? Accuracy { get; set; }

    public Category Category { get; set; }

    public int Range { get; set; }

    [NotMapped]
    public List<string> RangeModifiers { get; set; }
}

    public enum Frequency
    {
        AT_WILL,
        EOT,
        SCENE
    }

    public enum Category
    {
        PHYSICAL,
        SPECIAL,
        STATUS
    }

}
