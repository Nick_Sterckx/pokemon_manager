﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pokemonmanager.Migrations
{
    public partial class update_pokemon : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EggGroup1",
                table: "PokemonItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EggGroup2",
                table: "PokemonItems",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SkillsId",
                table: "PokemonItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "_Diets",
                table: "PokemonItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ActionType",
                table: "AbilityItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "AbilityItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Frequency",
                table: "AbilityItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AbilityItems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Athletics = table.Column<string>(nullable: true),
                    Acrobatics = table.Column<string>(nullable: true),
                    Combat = table.Column<string>(nullable: true),
                    Stealth = table.Column<string>(nullable: true),
                    Perception = table.Column<string>(nullable: true),
                    Focus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PokemonItems_SkillsId",
                table: "PokemonItems",
                column: "SkillsId");

            migrationBuilder.AddForeignKey(
                name: "FK_PokemonItems_Skills_SkillsId",
                table: "PokemonItems",
                column: "SkillsId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PokemonItems_Skills_SkillsId",
                table: "PokemonItems");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropIndex(
                name: "IX_PokemonItems_SkillsId",
                table: "PokemonItems");

            migrationBuilder.DropColumn(
                name: "EggGroup1",
                table: "PokemonItems");

            migrationBuilder.DropColumn(
                name: "EggGroup2",
                table: "PokemonItems");

            migrationBuilder.DropColumn(
                name: "SkillsId",
                table: "PokemonItems");

            migrationBuilder.DropColumn(
                name: "_Diets",
                table: "PokemonItems");

            migrationBuilder.DropColumn(
                name: "ActionType",
                table: "AbilityItems");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "AbilityItems");

            migrationBuilder.DropColumn(
                name: "Frequency",
                table: "AbilityItems");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AbilityItems");
        }
    }
}
