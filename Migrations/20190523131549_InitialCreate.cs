﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pokemonmanager.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AbilityItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AbilityItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PokemonItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Pokedex_id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type1 = table.Column<int>(nullable: false),
                    Type2 = table.Column<int>(nullable: false),
                    HP_Stat = table.Column<int>(nullable: false),
                    ATT_Stat = table.Column<int>(nullable: false),
                    DEF_Stat = table.Column<int>(nullable: false),
                    SATT_Stat = table.Column<int>(nullable: false),
                    SDEF_Stat = table.Column<int>(nullable: false),
                    SPD_Stat = table.Column<int>(nullable: false),
                    Height = table.Column<string>(nullable: true),
                    Weight = table.Column<string>(nullable: true),
                    GenderRatio = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PokemonItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AbilityLink",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<int>(nullable: false),
                    AbilityId = table.Column<long>(nullable: true),
                    PokemonId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AbilityLink", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AbilityLink_AbilityItems_AbilityId",
                        column: x => x.AbilityId,
                        principalTable: "AbilityItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AbilityLink_PokemonItems_PokemonId",
                        column: x => x.PokemonId,
                        principalTable: "PokemonItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MoveItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Frequency = table.Column<int>(nullable: false),
                    FrequencyTimes = table.Column<int>(nullable: true),
                    Accuracy = table.Column<int>(nullable: true),
                    Category = table.Column<int>(nullable: false),
                    Range = table.Column<int>(nullable: false),
                    PokemonId = table.Column<long>(nullable: true),
                    PokemonId1 = table.Column<long>(nullable: true),
                    PokemonId2 = table.Column<long>(nullable: true),
                    PokemonId3 = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoveItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MoveItems_PokemonItems_PokemonId",
                        column: x => x.PokemonId,
                        principalTable: "PokemonItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MoveItems_PokemonItems_PokemonId1",
                        column: x => x.PokemonId1,
                        principalTable: "PokemonItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MoveItems_PokemonItems_PokemonId2",
                        column: x => x.PokemonId2,
                        principalTable: "PokemonItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MoveItems_PokemonItems_PokemonId3",
                        column: x => x.PokemonId3,
                        principalTable: "PokemonItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AbilityLink_AbilityId",
                table: "AbilityLink",
                column: "AbilityId");

            migrationBuilder.CreateIndex(
                name: "IX_AbilityLink_PokemonId",
                table: "AbilityLink",
                column: "PokemonId");

            migrationBuilder.CreateIndex(
                name: "IX_MoveItems_PokemonId",
                table: "MoveItems",
                column: "PokemonId");

            migrationBuilder.CreateIndex(
                name: "IX_MoveItems_PokemonId1",
                table: "MoveItems",
                column: "PokemonId1");

            migrationBuilder.CreateIndex(
                name: "IX_MoveItems_PokemonId2",
                table: "MoveItems",
                column: "PokemonId2");

            migrationBuilder.CreateIndex(
                name: "IX_MoveItems_PokemonId3",
                table: "MoveItems",
                column: "PokemonId3");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AbilityLink");

            migrationBuilder.DropTable(
                name: "MoveItems");

            migrationBuilder.DropTable(
                name: "AbilityItems");

            migrationBuilder.DropTable(
                name: "PokemonItems");
        }
    }
}
