import { ClarityModule } from '@clr/angular';
import { MovesComponent } from './moves/moves.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovesRoutingModule } from './moves-routing.module';
import { MovesDetailComponent } from './moves-detail/moves-detail.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MovesComponent, MovesDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    MovesRoutingModule,
    ClarityModule
  ]
})
export class MovesModule { }
