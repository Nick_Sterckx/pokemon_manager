import { Move } from './../models/move';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestService } from '../shared/rest.service';

@Injectable({
  providedIn: 'root'
})
export class MovesService extends RestService<Move> {

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    super(http, baseUrl + 'api/moves');
   }
}
