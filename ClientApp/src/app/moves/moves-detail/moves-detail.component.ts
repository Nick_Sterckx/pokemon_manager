import { MovesService } from './../moves.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Move } from 'src/app/models/move';

@Component({
  selector: 'app-moves-detail',
  templateUrl: './moves-detail.component.html',
  styleUrls: ['./moves-detail.component.scss']
})
export class MovesDetailComponent implements OnInit {

  move: Move;
  loading: boolean = false;

  constructor(private route: ActivatedRoute, private moveService: MovesService, private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.route.params.subscribe((params) => {
      console.log(params);
      if (params['id']) {
        this.moveService.getById(params['id']).subscribe((res) => {
          console.log(res);
            this.move = res;
            this.loading = false;
        });
      } else {
        this.move = new Move();
        this.loading = false
      }
    })
  }

  submit() {
    if (this.move.id) {
      this.moveService.update(this.move, this.move.id).subscribe((res) => {
        console.log('updated move!');
      });
    } else {
      this.moveService.create(this.move).subscribe((res) => {
        console.log('created move!', res);
        this.router.navigate(['./moves/', res.id]);
      })
    }
  }

  delete() {
    this.moveService.delete(this.move.id).subscribe((res) => {
      console.log('hello?');
      this.router.navigate(['moves']);
    });
  }
}
