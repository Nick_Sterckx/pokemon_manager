import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovesDetailComponent } from './moves-detail.component';

describe('MovesDetailComponent', () => {
  let component: MovesDetailComponent;
  let fixture: ComponentFixture<MovesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
