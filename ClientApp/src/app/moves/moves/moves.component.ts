import { Type } from './../../models/type';
import { Move, Frequency, Category } from './../../models/move';
import { Component, OnInit } from '@angular/core';
import { MovesService } from '../moves.service';

@Component({
  selector: 'app-moves',
  templateUrl: './moves.component.html',
  styleUrls: ['./moves.component.css']
})
export class MovesComponent implements OnInit {

  moves: Array<Move> = [];
  frequencyEnum = Frequency;
  typeEnum = Type;
  categoryEnum = Category;

  constructor(private moveService: MovesService) { }

  ngOnInit() {
    this.moveService.getAll().subscribe((res) => {
      this.moves = res;
      console.log('moves!', res)
    });
  }

  getMoves() {

  }

}
