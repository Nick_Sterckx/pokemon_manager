import { MovesDetailComponent } from './moves-detail/moves-detail.component';
import { MovesComponent } from './moves/moves.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: '',
  component: MovesComponent
},
{
  path: 'new',
  component: MovesDetailComponent
},
{
  path: ':id',
  component: MovesDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovesRoutingModule { }
