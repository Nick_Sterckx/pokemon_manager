import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RestService<T> {

  constructor(public http: HttpClient, public url) { }

  public getAll() {
    return this.http.get<Array<T>>(this.url);
  }

  public getById(id: number) {
   return this.http.get<T>(this.url + '/' + id);
  }

  public create(item: T) {
     return this.http.post<T>(this.url, item);
  }

  public update(item: T, id: number) {
     return this.http.put<T>(this.url + '/' + id, item);
 }

 public delete(id: number) {
   return this.http.delete(this.url + '/' + id);
 }
}
