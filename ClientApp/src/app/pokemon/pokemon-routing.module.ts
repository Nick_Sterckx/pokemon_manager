import { PokemonComponent } from './pokemon/pokemon.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';

const routes: Routes = [{
  path: '',
  component: PokemonComponent
},
{
  path: 'new',
  component: PokemonDetailComponent
},
{
  path: ':id',
  component: PokemonDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PokemonRoutingModule { }
