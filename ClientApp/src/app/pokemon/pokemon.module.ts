import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokemonRoutingModule } from './pokemon-routing.module';
import { PokemonComponent } from './pokemon/pokemon.component';
import { ClarityModule } from '@clr/angular';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';

@NgModule({
  declarations: [PokemonComponent, PokemonDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    PokemonRoutingModule
  ]
})
export class PokemonModule { }
