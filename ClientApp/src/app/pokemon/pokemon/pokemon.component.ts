import { PokemonService } from './../pokemon.service';
import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';
import { Type } from 'src/app/models/type';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {

  items: Array<Pokemon> = [];

  typeEnum = Type;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {
    this.pokemonService.getAll().subscribe((res) => {
      this.items = res;
    });
  }

}
