import { HttpClient } from '@angular/common/http';
import { Pokemon } from './../models/pokemon';
import { RestService } from './../shared/rest.service';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PokemonService extends RestService<Pokemon> {

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    super(http, baseUrl + 'api/pokemons');
   }
}
