import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../pokemon.service';
import { Pokemon } from 'src/app/models/pokemon';

@Component({
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {

  pokemon: Pokemon = new Pokemon();

  loading: boolean = false;

  constructor(private route: ActivatedRoute, private pokemonService: PokemonService,
    private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.route.params.subscribe((params) => {
      if (params.id) {
        this.pokemonService.getById(params.id).subscribe((res) => {
          this.pokemon = res;
          this.loading = false;
        });
      } else {
        this.pokemon = new Pokemon();
        this.loading = false;
      }
    })
  }

  submit() {
    if (this.pokemon.id) {
      this.pokemonService.update(this.pokemon, this.pokemon.id).subscribe((res) => {
      });
    } else {
      this.pokemonService.create(this.pokemon).subscribe((res) => {
        this.router.navigate(['./pokemon', res.id]);
      });
    }
  }

  delete() {
    this.pokemonService.delete(this.pokemon.id).subscribe(() => {
      this.router.navigate(['pokemon']);
    });
  }

}
