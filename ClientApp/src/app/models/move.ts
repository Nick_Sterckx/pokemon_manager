import {Type} from './type';

export class Move {
  public id: number;
  public name: string;
  public description: string;
  public type: Type;
  public dB: number;
  public frequency: Frequency;
  public frequencyTimes: number;
  public accuracy: number;
  public category: Category;
  public range: number;
  public rangeModifiers: Array<string>;
}

export enum Frequency {
  AT_WILL,
  EOT,
  SCENE
}
export enum Category {
  PHYSICAL,
  SPECIAL,
  STATUS
}
