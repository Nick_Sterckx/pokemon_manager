import { Type } from './type';
import { Move } from './move';
import { Ability } from './ability';
import { Skills } from './skills';

export class Pokemon {
  public id: number;
  public pokedex_id: number;
  public name: string;
  public type1: Type;
  public type2: Type;
  public hp: number;
  public att: number;
  public def: number;
  public satt: number;
  public sdef: number;
  public spd: number;
  public abilities: AbilityLink[];
  public height: string;
  public weight: string;
  public genderRatio: number;
  public levelMoves: Move[];
  public tMMoves: Move[];
  public eggMove: Move[];
  public tutorMove: Move[];
  public eggGroup1: EggGroup;
  public eggGroup2: EggGroup;
  public diets: Diet[];

  public habitats: Habitat[];
  public skills: Skills;
}
export class AbilityLink {
  public id: number;
  public type: AbilityType;
  public ability: Ability;
}
export enum AbilityType {
  BASIC,
  ADVANCED,
  HIGH
}
export class EvolutionInfo {
  public Id: number;
  public Pokedex_id: number;
  public EvolutionType: EvolutionType;
  public Info: string;
}
export enum EvolutionType {
  LEVEL,
  STONE,
  OTHER
}
export enum EggGroup {
  MONSTER,
  WATER_1,
  BUG,
  FLYING,
  FIELD,
  FAIRY,
  GRASS,
  HUMANLIKE,
  WATER_3,
  MINERAL,
  AMORPHOUS,
  WATER_2,
  DITTO,
  DRAGON,
  UNDISCOVERED
}
export enum Diet {
  CARNIVORE,
  HERBIVORE,
  OMNIVORE,
  ERGOVORE,
  TERRAVORE,
  NULLIVORE
}
export enum Habitat {
  OCEAN,
  ARCTIC,
  TUNDRA,
  CAVE,
  TAIGA,
  FOREST,
  URBAN,
  GRASSLAND,
  BEACH,
  DESERT,
  MOUNTAIN,
  RAINFOREST
}
