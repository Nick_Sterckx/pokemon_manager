export class Skills {
  public Id: number;
  public Athletics: string;
  public Acrobatics: string;
  public Combat: string;
  public Stealth: string;
  public Perception: string;
  public Focus: string;
}
