import {Frequency} from './move'

    export class Ability {
  public Id: number;
  public Name: string;
  public Description: string;
  public Frequency: Frequency;
  public ActionType: ActionType;
}

export enum ActionType {
  FREE,

  SHIFT,

  STANDARD,

  FULL
}
