﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pokemon_manager.Models;

namespace Pokemon_manager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PokemonsController : ControllerBase
    {
        private readonly PokemonManagerContext _context;

        public PokemonsController(PokemonManagerContext context)
        {
            _context = context;
        }

        // GET: api/Pokemons
        [HttpGet]
        public IEnumerable<Pokemon> GetPokemonItems()
        {
            return _context.PokemonItems;
        }

        // GET: api/Pokemons/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPokemon([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pokemon = await _context.PokemonItems.FindAsync(id);

            if (pokemon == null)
            {
                return NotFound();
            }

            return Ok(pokemon);
        }

        // PUT: api/Pokemons/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPokemon([FromRoute] long id, [FromBody] Pokemon pokemon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pokemon.Id)
            {
                return BadRequest();
            }

            _context.Entry(pokemon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PokemonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pokemons
        [HttpPost]
        public async Task<IActionResult> PostPokemon([FromBody] Pokemon pokemon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PokemonItems.Add(pokemon);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPokemon", new { id = pokemon.Id }, pokemon);
        }

        // DELETE: api/Pokemons/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePokemon([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pokemon = await _context.PokemonItems.FindAsync(id);
            if (pokemon == null)
            {
                return NotFound();
            }

            _context.PokemonItems.Remove(pokemon);
            await _context.SaveChangesAsync();

            return Ok(pokemon);
        }

        private bool PokemonExists(long id)
        {
            return _context.PokemonItems.Any(e => e.Id == id);
        }
    }
}