﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pokemon_manager.Models;

namespace Pokemon_manager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovesController : ControllerBase
    {
        private PokemonManagerContext _context;

        public MovesController(PokemonManagerContext context)
        {
            _context = context;
        }

        // GET: api/Moves
        [HttpGet]
        public IEnumerable<Move> GetmoveItems()
        {
            return _context.MoveItems;
        }

        // GET: api/Moves/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMove([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var move = await _context.MoveItems.FindAsync(id);

            if (move == null)
            {
                return NotFound();
            }

            return Ok(move);
        }

        // PUT: api/Moves/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMove([FromRoute] long id, [FromBody] Move move)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != move.Id)
            {
                return BadRequest();
            }

            _context.Entry(move).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MoveExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Moves
        [HttpPost]
        public async Task<IActionResult> PostMove([FromBody] Move move)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MoveItems.Add(move);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMove", new { id = move.Id }, move);
        }

        // DELETE: api/Moves/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMove([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var move = await _context.MoveItems.FindAsync(id);
            if (move == null)
            {
                return NotFound();
            }

            _context.MoveItems.Remove(move);
            await _context.SaveChangesAsync();

            return Ok(move);
        }

        private bool MoveExists(long id)
        {
            return _context.MoveItems.Any(e => e.Id == id);
        }
    }
}